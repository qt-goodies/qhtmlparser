import qbs

Group {
    name: "QHtmlParser"
    prefix: path + "/src/"
    files: [
        "Html/entities.h",
        "Html/html.cpp",
        "Html/html.h",
        "Html/parser.cpp",
        "Html/parser.h",
    ]
}
