/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QHtmlParser.
 *
 * QHtmlParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QHtmlParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IT_MARDY_HTML_H
#define IT_MARDY_HTML_H

#include <QString>

namespace it {
namespace mardy {
namespace Html {

QString unescape(const QString &s);

}}} // namespace

#endif // IT_MARDY_HTML_H
