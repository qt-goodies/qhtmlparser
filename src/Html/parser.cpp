/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QHtmlParser.
 *
 * QHtmlParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QHtmlParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"

#include "html.h"

#include <QDebug>
#include <QRegularExpression>

namespace {

/* Regular expressions used for parsing */

const QString s_interestingNormal = QStringLiteral("[&<]");
const QString s_incomplete = QStringLiteral("&[a-zA-Z#]");

const QString s_entityRef =
    QStringLiteral("&([a-zA-Z][-.a-zA-Z0-9]*)[^a-zA-Z0-9]");
const QString s_charRef =
    QStringLiteral("&#(?:[0-9]+|[xX][0-9a-fA-F]+)[^0-9a-fA-F]");

const QString s_startTagOpen = QStringLiteral("<[a-zA-Z]");
const QString s_piClose = QStringLiteral(">");
const QString s_commentClose = QStringLiteral(R"(--\s*>)");
/* Note:
 *  1) if you change tagfind/attrfind remember to update locatestarttagend too;
 *  2) if you change tagfind/attrfind and/or locatestarttagend the parser will
 *     explode, so don't do it.
 * see http://www.w3.org/TR/html5/tokenization.html#tag-open-state
 * and http://www.w3.org/TR/html5/tokenization.html#tag-name-state
 */
const QString s_tagFindTolerant =
    QStringLiteral(R"(([a-zA-Z][^\t\n\r\f />\x00]*)(?:\s|/(?!>))*)");
const QString s_attrFindTolerant = QStringLiteral(
    R"(((?<=[\'"\s/])[^\s/>][^\s/=>]*)(\s*=+\s*)"
    R"((\'[^\']*\'|"[^"]*"|(?![\'"])[^>\s]*))?(?:\s|/(?!>))*)");
const QString s_locateStartTagEndTolerant = QStringLiteral(R"(
  <[a-zA-Z][^\t\n\r\f />\x00]*       # tag name
  (?:[\s/]*                          # optional whitespace before attribute name
    (?:(?<=['"\s/])[^\s/>][^\s/=>]*  # attribute name
      (?:\s*=+\s*                    # value indicator
        (?:'[^']*'                   # LITA-enclosed value
          |"[^"]*"                   # LIT-enclosed value
          |(?!['"])[^>\s]*           # bare value
         )
        \s*                          # possibly followed by a space
       )?(?:\s|/(?!>))*
     )*
   )?
  \s*                                # trailing whitespace
)");
const QString s_endEndTag = QStringLiteral(">");
/* the HTML 5 spec, section 8.1.2.2, doesn't allow spaces between
 * </ and the tag name, so maybe this should be fixed
 */
const QString s_endTagFind = QStringLiteral(R"(</\s*([a-zA-Z][-.a-zA-Z0-9:_]*)\s*>)");

// From _markupbase.py:
const QString s_declName = QStringLiteral(R"([a-zA-Z][-_.a-zA-Z0-9]*\s*)");
const QString s_markedSectionClose = QStringLiteral(R"(]\s*]\s*>)");
const QString s_msMarkedSectionClose = QStringLiteral(R"(]\s*>)");

struct RegularExpressions {
    QRegularExpression interestingNormal;
    QRegularExpression incomplete;
    QRegularExpression entityRef;
    QRegularExpression charRef;
    QRegularExpression startTagOpen;
    QRegularExpression piClose;
    QRegularExpression commentClose;
    QRegularExpression tagFindTolerant;
    QRegularExpression attrFindTolerant;
    QRegularExpression locateStartTagEndTolerant;
    QRegularExpression endEndTag;
    QRegularExpression endTagFind;
    QRegularExpression declName;
    QRegularExpression markedSectionClose;
    QRegularExpression msMarkedSectionClose;

    void init() {
        interestingNormal = QRegularExpression(s_interestingNormal);
        incomplete = QRegularExpression(s_incomplete);
        entityRef = QRegularExpression(s_entityRef);
        charRef = QRegularExpression(s_charRef);
        startTagOpen = QRegularExpression(s_startTagOpen);
        piClose = QRegularExpression(s_piClose);
        commentClose = QRegularExpression(s_commentClose);
        tagFindTolerant = QRegularExpression(s_tagFindTolerant);
        attrFindTolerant = QRegularExpression(s_attrFindTolerant);
        locateStartTagEndTolerant =
            QRegularExpression(s_locateStartTagEndTolerant,
                               QRegularExpression::ExtendedPatternSyntaxOption);
        endEndTag = QRegularExpression(s_endEndTag);
        endTagFind = QRegularExpression(s_endTagFind);
        declName = QRegularExpression(s_declName);
        markedSectionClose = QRegularExpression(s_markedSectionClose);
        msMarkedSectionClose = QRegularExpression(s_msMarkedSectionClose);
    };
} s_re;

} // namespace

using namespace it::mardy::Html;

namespace it {
namespace mardy {
namespace Html {

class ParserPrivate
{
public:
    ParserPrivate(Parser *q, Parser::Options options);

    void reset();

    void goAhead(bool isEnd);

    void setCDataMode(const QString &elem);
    void clearCDataMode();

private:
    int parseStartTag(int i);
    int parseEndTag(int i);
    int parsePi(int i);
    int parseHtmlDeclaration(int i);
    int parseBogusComment(int i, bool mustReport = true);

    /* methods from _markupbase.py */
    int updatePos(int i, int j);
    int parseComment(int i, bool mustReport = true);
    int parseMarkedSection(int i, bool mustReport = true);
    int checkForWholeStartTag(int i);
    QString scanName(int i, int declStartPos, int *endPos);

private:
    Q_DECLARE_PUBLIC(Parser)

    static QStringList s_cdataContentElements;
    bool m_convertCharRefs;
    QString m_rawData;
    QString m_lastTag;
    QRegularExpression m_interesting;
    QString m_cdataElem;

    QString m_startTagText; // in Python, it's a class attribute: why?

    // from _markupbase.py:
    int m_lineNo;
    int m_offset;

    Parser *q_ptr;
};

}}} // namespace

QStringList ParserPrivate::s_cdataContentElements = { "script", "style" };

ParserPrivate::ParserPrivate(Parser *q, Parser::Options options):
    m_convertCharRefs(options & Parser::ConvertCharRefs),
    m_lineNo(0),
    m_offset(0),
    q_ptr(q)
{
    s_re.init();
}

void ParserPrivate::reset()
{
    m_rawData.clear();
    m_lastTag = "???";
    m_interesting = s_re.interestingNormal;
    m_cdataElem.clear();
    // from _markupbase.py:
    m_lineNo = 1;
    m_offset = 0;
}

/* Internal -- handle data as far as reasonable.  May leave state
 * and data to be processed by a subsequent call.  If 'end' is
 * true, force handling all data as if followed by EOF marker.
 */
void ParserPrivate::goAhead(bool isEnd)
{
    Q_Q(Parser);

    const QString &rawData = m_rawData;
    int i = 0;
    int j = 0;
    int n = rawData.length();
    while (i < n) {
        if (m_convertCharRefs && m_cdataElem.isEmpty()) {
            j = rawData.indexOf('<', i);
            if (j < 0) {
                /* if we can't find the next <, either we are at the end
                 * or there's more text incoming.  If the latter is True,
                 * we can't pass the text to handle_data in case we have
                 * a charref cut in half at end.  Try to determine if
                 * this is the case before proceeding by looking for an
                 * & near the end and see if it's followed by a space or ;.
                 */
                int ampPos = rawData.indexOf('&', qMax(i, n - 34));
                if (ampPos >= 0 &&
                    !QRegularExpression(R"([\s;])").match(rawData, ampPos).hasMatch()) {
                    break; // wait till we get all the text
                }
                j = n;
            }
        } else {
            auto match = m_interesting.match(rawData, i); // < or &
            if (match.hasMatch()) {
                j = match.capturedStart();
            } else {
                if (!m_cdataElem.isEmpty()) {
                    break;
                }
                j = n;
            }
        }

        if (i < j) {
            if (m_convertCharRefs && m_cdataElem.isEmpty()) {
                q->handleData(unescape(rawData.mid(i, j - i)));
            } else {
                q->handleData(rawData.mid(i, j - i));
            }
        }

        i = updatePos(i, j);
        if (i == n) break;
        QStringRef rawDataAtI = rawData.midRef(i);
        if (rawDataAtI.startsWith('<')) {
            int k = 0;
            if (s_re.startTagOpen.match(rawData, i,
                                        QRegularExpression::NormalMatch,
                                        QRegularExpression::AnchoredMatchOption).
                hasMatch()) { // < + letter
                k = parseStartTag(i);
            } else if (rawDataAtI.startsWith("</")) {
                k = parseEndTag(i);
            } else if (rawDataAtI.startsWith("<!--")) {
                k = parseComment(i);
            } else if (rawDataAtI.startsWith("<?")) {
                k = parsePi(i);
            } else if (rawDataAtI.startsWith("<!")) {
                k = parseHtmlDeclaration(i);
            } else if (i + 1 < n) {
                q->handleData("<");
                k = i + 1;
            } else {
                break;
            }

            if (k < 0) {
                if (!isEnd) {
                    break;
                }
                k = rawData.indexOf('>', i + 1);
                if (k < 0) {
                    k = rawData.indexOf('<', i + 1);
                    if (k < 0) {
                        k = i + 1;
                    }
                } else {
                    k += 1;
                }

                if (m_convertCharRefs && m_cdataElem.isEmpty()) {
                    q->handleData(unescape(rawData.mid(i, k - i)));
                } else {
                    q->handleData(rawData.mid(i, k - i));
                }
            }

            i = updatePos(i, k);
        } else if (rawDataAtI.startsWith("&#")) {
            auto match = s_re.charRef.match(rawData, i,
                                            QRegularExpression::NormalMatch,
                                            QRegularExpression::AnchoredMatchOption);
            if (match.hasMatch()) {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
                QString name = match.captured().mid(2).chopped(1);
#else
                QString name = match.captured().mid(2); name.chop(1);
#endif
                q->handleCharRef(name);
                int k = match.capturedEnd();
                if (!rawData.midRef(k - 1).startsWith(';')) {
                    k = k - 1;
                }
                i = updatePos(i, k);
                continue;
            } else {
                if (rawDataAtI.contains(';')) { // bail by consuming &#
                    q->handleData(rawData.mid(i, 2));
                    i = updatePos(i, i + 2);
                }
                break;
            }
        } else if (rawDataAtI.startsWith('&')) {
            auto match = s_re.entityRef.match(rawData, i,
                                              QRegularExpression::NormalMatch,
                                              QRegularExpression::AnchoredMatchOption);
            if (match.hasMatch()) {
                QString name = match.captured(1);
                q->handleEntityRef(name);
                int k = match.capturedEnd();
                if (!rawData.midRef(k - 1).startsWith(';')) {
                    k = k - 1;
                }
                i = updatePos(i, k);
                continue;
            }

            match = s_re.incomplete.match(rawData, i,
                                          QRegularExpression::NormalMatch,
                                          QRegularExpression::AnchoredMatchOption);
            if (match.hasMatch()) {
                // match.group() will contain at least 2 chars
                if (isEnd && match.captured() == rawDataAtI) {
                    int k = match.capturedEnd();
                    if (k <= i) {
                        k = n;
                    }
                    i = updatePos(i, i + 1);
                }
                // incomplete
                break;
            } else if (i + 1 < n) {
                /* not the end of the buffer, and can't be confused
                 * with some other construct
                 */
                q->handleData("&");
                i = updatePos(i, i + 1);
            } else {
                break;
            }
        } else {
            qFatal("interesting.search() lied");
        }
    } // end while

    if (isEnd && i < n && m_cdataElem.isEmpty()) {
        if (m_convertCharRefs && m_cdataElem.isEmpty()) {
            q->handleData(unescape(rawData.mid(i, n - i)));
        } else {
            q->handleData(rawData.mid(i, n - i));
        }
        i = updatePos(i, n);
    }

    m_rawData = rawData.mid(i);
}

void ParserPrivate::setCDataMode(const QString &elem)
{
    m_cdataElem = elem.toLower();
    m_interesting =
        QRegularExpression(QString(R"(</\s*%1\s*>)").arg(m_cdataElem),
                           QRegularExpression::CaseInsensitiveOption);
}

void ParserPrivate::clearCDataMode()
{
    m_interesting = s_re.interestingNormal;
    m_cdataElem.clear();
}

/* parse html declarations, return length or -1 if not terminated
 * See w3.org/TR/html5/tokenization.html#markup-declaration-open-state
 * See also parse_declaration in _markupbase
 */
int ParserPrivate::parseHtmlDeclaration(int i)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X(rawData.midRef(i, 2) == "<!", "parser",
               "unexpected call to parseHtmlDeclaration");
    if (rawData.midRef(i, 4) == "<!--") {
        // this case is actually already handled in goahead()
        return parseComment(i);
    } else if (rawData.midRef(i, 3) == "<![") {
        return parseMarkedSection(i);
    } else if (rawData.mid(i, 9).toLower() == "<!doctype") {
        // find the closing >
        int gtpos = rawData.indexOf('>', i + 9);
        if (gtpos == -1) {
            return -1;
        }
        q->handleDecl(rawData.mid(i + 2, gtpos - (i + 2)));
        return gtpos + 1;
    } else {
        return parseBogusComment(i);
    }
}

/* Internal -- parse bogus comment, return length or -1 if not terminated
 * see http://www.w3.org/TR/html5/tokenization.html#bogus-comment-state
 */
int ParserPrivate::parseBogusComment(int i, bool mustReport)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X((rawData.midRef(i, 2) == "<!" ||
                rawData.midRef(i, 2) == "</"), "parser",
               "unexpected call to parseComment()");
    int pos = rawData.indexOf('>', i + 2);
    if (pos == -1) {
        return -1;
    }
    if (mustReport) {
        q->handleComment(rawData.mid(i + 2, pos - (i + 2)));
    }
    return pos + 1;
}

/* Internal -- parse processing instr, return end or -1 if not terminated
 */
int ParserPrivate::parsePi(int i)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X(rawData.midRef(i, 2) == "<?", "parser",
               "unexpected call to parsePi()");
    auto match = s_re.piClose.match(rawData, i + 2); // >
    if (!match.hasMatch()) {
        return -1;
    }
    int j = match.capturedStart();
    q->handlePi(rawData.mid(i + 2, j - (i + 2)));
    j = match.capturedEnd();
    return j;
}

/* Internal -- handle starttag, return end or -1 if not terminated
 */
int ParserPrivate::parseStartTag(int i)
{
    Q_Q(Parser);
    m_startTagText.clear();
    int endPos = checkForWholeStartTag(i);
    if (endPos < 0) {
        return endPos;
    }
    const QString &rawData = m_rawData;
    m_startTagText = rawData.mid(i, endPos - i);

    // Now parse the data between i+1 and j into a tag and attrs
    Parser::Attributes attrs;
    auto match = s_re.tagFindTolerant.match(rawData, i + 1,
            QRegularExpression::NormalMatch,
            QRegularExpression::AnchoredMatchOption);
    Q_ASSERT_X(match.hasMatch(), "parseStartTag",
               "unexpected call to parseStartTag()");
    int k = match.capturedEnd();
    QString tag = m_lastTag = match.captured(1).toLower();
    while (k < endPos) {
        auto m = s_re.attrFindTolerant.match(rawData, k,
                QRegularExpression::NormalMatch,
                QRegularExpression::AnchoredMatchOption);
        if (!m.hasMatch()) {
            break;
        }
        QString attrName = m.captured(1);
        QString rest = m.captured(2);
        QString attrValue = m.captured(3);
        if (rest.isEmpty()) {
            attrValue.clear();
        } else if (
            (attrValue.leftRef(1) == '\'' && '\'' == attrValue.rightRef(1)) ||
            (attrValue.leftRef(1) == '"' && '"' == attrValue.rightRef(1))) {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
            attrValue = attrValue.midRef(1).chopped(1).toString();
#else
            attrValue = attrValue.mid(1); attrValue.chop(1);
#endif
        }
        if (!attrValue.isEmpty()) {
            attrValue = unescape(attrValue);
        }
        attrs.append({attrName.toLower(), attrValue});
        k = m.capturedEnd();
    }

    QStringRef end = rawData.midRef(k, endPos - k).trimmed();
    if (end != ">" && end != "/>") {
        q->handleData(rawData.mid(i, endPos - i));
        return endPos;
    }
    if (end.endsWith("/>")) {
        // XHTML-style empty tag: <span attr="value" />
        q->handleStartEndTag(tag, attrs);
    } else {
        q->handleStartTag(tag, attrs);
        if (s_cdataContentElements.contains(tag)) {
            setCDataMode(tag);
        }
    }
    return endPos;
}

int ParserPrivate::checkForWholeStartTag(int i)
{
    const QString &rawData = m_rawData;
    auto m = s_re.locateStartTagEndTolerant.match(rawData, i,
            QRegularExpression::NormalMatch,
            QRegularExpression::AnchoredMatchOption);
    if (m.hasMatch()) {
        int j = m.capturedEnd();
        QStringRef next = rawData.midRef(j, 1);
        if (next == ">") {
            return j + 1;
        }
        if (next == "/") {
            QStringRef rawDataAtJ = rawData.midRef(j);
            if (rawDataAtJ.startsWith("/>")) {
                return j + 2;
            }
            if (rawDataAtJ.startsWith("/")) {
                // buffer boundary
                return -1;
            }
            // else bogus input
            if (j > i) {
                return j;
            } else {
                return i + 1;
            }
        }
        if (next.isEmpty()) {
            // end of input
            return -1;
        }
        static const QString charList =
            QStringLiteral("abcdefghijklmnopqrstuvwxyz=/"
                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        if (charList.contains(next)) {
            /* end of input in or before attribute value, or we have the
             * '/' from a '/>' ending
             */
            return -1;
        }
        if (j > i) {
            return j;
        } else {
            return i + 1;
        }
    }
    qFatal("we should not get here!");
}

/* Internal -- handle starttag, return end or -1 if not terminated
 */
int ParserPrivate::parseEndTag(int i)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X(rawData.midRef(i, 2) == "</", "parseEndTag",
               "unexpected call to parse_endtag");
    auto match = s_re.endEndTag.match(rawData, i + 1); // >
    if (!match.hasMatch()) {
        return -1;
    }
    int gtpos = match.capturedEnd();
    match = s_re.endTagFind.match(rawData, i,
            QRegularExpression::NormalMatch,
            QRegularExpression::AnchoredMatchOption); // </ + tag + >
    if (!match.hasMatch()) {
        if (!m_cdataElem.isEmpty()) {
            q->handleData(rawData.mid(i, gtpos - i));
            return gtpos;
        }
        // find the name: w3.org/TR/html5/tokenization.html#tag-name-state
        auto nameMatch = s_re.tagFindTolerant.match(rawData, i + 2,
                QRegularExpression::NormalMatch,
                QRegularExpression::AnchoredMatchOption);
        if (!nameMatch.hasMatch()) {
            // w3.org/TR/html5/tokenization.html#end-tag-open-state
            if (rawData.midRef(i, 3) == "</>") {
                return i + 3;
            } else {
                return parseBogusComment(i);
            }
        }
        QString tagName = nameMatch.captured(1).toLower();
        /* consume and ignore other stuff between the name and the >
         * Note: this is not 100% correct, since we might have things like
         * </tag attr=">">, but looking for > after tha name should cover
         * most of the cases and is much simpler
         */
        gtpos = rawData.indexOf('>', nameMatch.capturedEnd());
        q->handleEndTag(tagName);
        return gtpos + 1;
    }

    QString elem = match.captured(1).toLower(); // script or style
    if (!m_cdataElem.isEmpty()) {
        if (elem != m_cdataElem) {
            q->handleData(rawData.mid(i, gtpos - i));
            return gtpos;
        }
    }

    q->handleEndTag(elem);
    clearCDataMode();
    return gtpos;
}

int ParserPrivate::updatePos(int i, int j)
{
    if (i >= j) {
        return j;
    }
    const QString &rawData = m_rawData;
    int nLines = rawData.midRef(i, j - i).count('\n');
    if (nLines != 0) {
        m_lineNo = m_lineNo + nLines;
        int pos = rawData.leftRef(j).lastIndexOf('\n', i); // Should not fail
        m_offset = j - (pos + 1);
    } else {
        m_offset = m_offset + j - i;
    }
    return j;
}

int ParserPrivate::parseComment(int i, bool mustReport)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X(rawData.midRef(i, 4) == "<!--", "parseComment",
               "unexpected call to parseComment()");
    auto match = s_re.commentClose.match(rawData, i + 4);
    if (!match.hasMatch()) {
        return -1;
    }
    if (mustReport) {
        int j = match.capturedStart();
        q->handleComment(rawData.mid(i + 4, j - (i + 4)));
    }
    return match.capturedEnd();
}

/* Internal -- parse a marked section
 * Override this to handle MS-word extension syntax <![if word]>content<![endif]>
 */
int ParserPrivate::parseMarkedSection(int i, bool mustReport)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    Q_ASSERT_X(rawData.midRef(i, 3) == "<![", "parseMarkedSection",
               "unexpected call to parseMarkedSection()");
    int j = 0;
    QString sectName = scanName(i + 3, i, &j);
    if (j < 0) {
        return j;
    }
    static const QStringList standardClosing {
        "temp", "cdata", "ignore", "include", "rcdata",
    };
    static const QStringList msClosing { "if", "else", "endif" };
    QRegularExpressionMatch match;
    if (standardClosing.contains(sectName)) {
        // look for standard ]]> ending
        match = s_re.markedSectionClose.match(rawData, i + 3);
    } else if (msClosing.contains(sectName)) {
        // look for MS Office ]> ending
        match = s_re.msMarkedSectionClose.match(rawData, i + 3);
    } else {
        q->handleError(QString("unknown status keyword %1 in marked section").
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
                       arg(rawData.midRef(i + 3, j - (i + 3))));
#else
                       arg(rawData.midRef(i + 3, j - (i + 3)).toString()));
#endif
    }
    if (!match.hasMatch()) {
        return -1;
    }
    if (mustReport) {
        int j = match.capturedStart();
        q->handleUnknownDecl(rawData.mid(i + 3, j - (i + 3)));
    }
    return match.capturedEnd();
}

QString ParserPrivate::scanName(int i, int declStartPos, int *endPos)
{
    Q_Q(Parser);
    const QString &rawData = m_rawData;
    int n = rawData.length();
    if (i == n) {
        *endPos = -1;
        return QString();
    }
    auto m = s_re.declName.match(rawData, i,
            QRegularExpression::NormalMatch,
            QRegularExpression::AnchoredMatchOption);
    if (m.hasMatch()) {
        QString s = m.captured();
        QString name = s.trimmed();
        if (i + s.length() == n) {
            *endPos = -1;
            return QString(); // end of buffer
        }
        *endPos = m.capturedEnd();
        return name.toLower();
    } else {
        updatePos(declStartPos, i);
        q->handleError(QString("expected name token at %1").
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
                       arg(rawData.midRef(declStartPos, 20)));
#else
                       arg(rawData.midRef(declStartPos, 20).toString()));
#endif
        *endPos = -1;
        return QString();
    }
}

Parser::Parser(Options options):
    d_ptr(new ParserPrivate(this, options))
{
    reset();
}

Parser::~Parser() = default;

void Parser::reset()
{
    Q_D(Parser);
    d->reset();
}

void Parser::feed(const QString &data)
{
    Q_D(Parser);
    d->m_rawData += data;
    d->goAhead(false);
}

void Parser::close()
{
    Q_D(Parser);
    d->goAhead(true);
}

QString Parser::startTagText() const
{
    Q_D(const Parser);
    return d->m_startTagText;
}

void Parser::handleStartEndTag(const QString &tag, const Attributes &attrs)
{
    handleStartTag(tag, attrs);
    handleEndTag(tag);
}

void Parser::handleStartTag(const QString &, const Attributes &)
{
}

void Parser::handleEndTag(const QString &)
{
}

void Parser::handleCharRef(const QString &)
{
}

void Parser::handleEntityRef(const QString &)
{
}

void Parser::handleData(const QString &)
{
}

void Parser::handleComment(const QString &)
{
}

void Parser::handleDecl(const QString &)
{
}

void Parser::handlePi(const QString &)
{
}

void Parser::handleUnknownDecl(const QString &)
{
}

void Parser::handleError(const QString &message)
{
    qWarning() << "HTML parser error:" << message;
}
