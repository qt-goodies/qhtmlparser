import qbs 1.0

Project {
    name: "QHtmlParser"

    property bool buildTests: false
    property bool enableCoverage: false

    qbsSearchPaths: "qbs"

    references: [
        "tests/tests.qbs",
    ]

    AutotestRunner {
        name: "check"
        Depends { productTypes: ["coverage-clean"] }
    }
}
